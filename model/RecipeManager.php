<?php
class RecipeManager {
    private $_db;

    public function __construct(string $path)
    {
        $db=new PDO('sqlite:'.$path.'/cookbook.sqlite');
        $db->exec("CREATE TABLE IF NOT EXISTS recipes (id INTEGER PRIMARY KEY, title TEXT, author TEXT, ingredients TEXT, instructions TEXT, is_vegetarian INTEGER, is_vegan INTEGER, needs_oven INTEGER)");    
        $this->setDb($db);
    }

    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }

    public function save(Recipe $recipe)
    {
        if ($recipe->isValid()) {
            $attribute_values=array();
            foreach (Recipe::attributeKeys() as $attribute)
            {
                $method = 'get'.ucfirst($attribute);
                if (method_exists($recipe, $method))
                {
                    $value= $recipe->$method();
                    $attribute_values += [$attribute => $value ];
                }
            }
            $provided_info=implode(", ", Recipe::attributeKeys());
            $parameters_names=":".implode(", :", Recipe::attributeKeys());
            $request = $this->_db->prepare('INSERT INTO recipes ('.$provided_info.') VALUES ('.$parameters_names.')');
            $execution = $request->execute($attribute_values);
            $recipe->setId($this->_db->lastInsertId());
        }
    }

    public function search(Array $search) //renvoie un array de Recipes
    {
        $query="SELECT * FROM recipes";
        if (count($search)>0){
            $search_criteria=array();
            $title_and_ingredients=array();
            if ($search['vege_status']=='vegan'){
                $search_criteria[]="is_vegan IS 1";
            }
            elseif ($search['vege_status']=='vegetarian') {
                $search_criteria[]="((is_vegetarian IS 1) OR (is_vegan IS 1))";
            }
            if ($search['needs_oven']==='true'){
                $search_criteria[]="needs_oven IS 1";
            }
            elseif ($search['needs_oven']==='false') {
                $search_criteria[]="needs_oven IS 0";
            }
            if ($search['title']!=''){
                $search_criteria[]="LOWER(title) LIKE :title";
                $title_and_ingredients['title']='%'.strtolower($_POST['title']).'%';
            }
            if ($search['ingredients']!=''){
                $search_criteria[]="LOWER(ingredients) LIKE :ingredients";
                $title_and_ingredients['ingredients']='%'.strtolower($_POST['ingredients']).'%';
            }
            if (count($search_criteria)>0){
            $query.=" WHERE ".implode(" AND ",$search_criteria);
            }
        }
        $request_result =$this->_db->prepare($query);
        $request_result->execute($title_and_ingredients);
        $results=array();
        while ($parameters=$request_result->fetch())
        {
            $results[]= new Recipe($parameters);
        }
        return $results;
    }

    public function loadAll()
    {
        $request_results= $this->_db->query("SELECT * FROM recipes"); 
        $results=array();
        while ($parameters=$request_results->fetch())
        {
            $results[]= new Recipe($parameters);
        }
        return $results ;
    }

    public function load(int $id)
    {
        $search=$this->_db->query("SELECT * FROM recipes WHERE id=$id");
        while ($parameters = $search->fetch()) {
            return new Recipe($parameters);
        }
    }
}
