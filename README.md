# Contexte

J'ai chez moi un carnet dans lequel je note succinctement les recettes que j'aime bien et que je ne connais pas par coeur. Souci : ce carnet ne peut pas être à deux endroits à la fois, donc c'est compliqué si je veux y accéder sans être chez moi.

Je voudrais donc arriver à en faire une version en ligne. Au passage, c'est aussi l'occasion pour moi de me mettre à php et à utiliser git.

# Fonctionnalités

## Actuelles
- affichage des recettes à partir d'une base de données
- ajout d'une recette dans la BDD à partir d'un formulaire
- recherche simple dans la BDD à partir de plusieurs critères

## Souhaitées
- design responsif
- Mise en place de comptes utilisateurice avec mdp
- possibilité d'éditer/supprimer ses propres recettes
- possibilité de copier la recette de quelqu'un pour la modifier.

## Non envisagées
- photos des recettes
- notation

