<?php
require_once('model/Recipe.php');
require_once('model/RecipeManager.php');
require_once('model/Search.php');

$recipe_list=new RecipeManager(__DIR__);

$target='add_recipe.php';
$recipe= new Recipe($_POST);
$recipe_list->save($recipe);
$empty_recipe=new Recipe([]);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Ajouter une recette : </title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <h1>Ajouter une recette</h1>
        <div>
            <?php
            if ($recipe == $empty_recipe)
            {
                echo "Veuillez saisir votre recette :";
                echo $recipe->viewForm($target); 
            }
            elseif ($recipe->isValid()==False)
            {
                echo "Votre recette n'est pas valide, veuillez la corriger";
                echo $recipe->viewForm($target);
            }
            else 
            {
                echo "Voici la recette que vous avez enregistrée : ( <a href='add_recipe.php'>Ajouter une autre recette</a> ou <a href='index.php'>revenir à l'accueil</a>)";
                echo $recipe->viewHtml();
            }
            ?>
        </div>
    </body>
</html>
